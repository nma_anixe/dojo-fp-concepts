// * Functional Programming Concepts

// Something to start with ;)
const log = console.log;

function calculateResult(data): number {
  if (data.type === "one") {
    return data.value + 1;
  } else if (data.type === "two") {
    return data.value + 2;
  } else {
    throw new Error("unsupported");
  }
}

/**
 * * Exercises
 */

// ! Single Object
const data = { type: "one", value: 5 };

// ? Exercise 1
// log output of calculateResult in the form of { result: ... }
// Note: ignore Exceptions

// ? Exercise 2
// Do exercise 1 using either a class or a function constructor

// ? Exercise 3
// Do exercise 1 using a static 'of' class member function

// ? Exercise 4
// Do exercise 1 without using composition or OOP

// ! Array of Objects
const lotsOfData = [
  { type: "one", value: 4 },
  { type: "one", value: 6 },
];

// ? Exercise 5
// Do exercise 1 for lotsOfData (output should be in array form i.e [ {...}, {...} ])

// ? Exercise 6
// Do exercise 5 using Array.map

// ? Exercise 7
// Do exercise 6 using the following pipe function
const pipe = (...fns) => (x) => fns.reduce((res, fn) => fn(res), x);

// ? Exercise 8
// Do exercise 5 and log the average of calculated values
