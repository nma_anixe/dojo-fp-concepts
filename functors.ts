// * FUNCTORS

/**
 * * Characteristics
 * 
 * - type agnostic
 * - composable
 * - mappers
 * 
 */

// Example -- Food for thought
export const Identity = value => ({
  map: fn => Identity(fn(value)),
  valueOf: () => value
})
