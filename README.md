# DOJO - Functional Programming Concepts - Part 1

1. Functions as Objects
2. Composition
4. Partial Functions & Currying
5. Pure Functions
<br>--- *Up to here* ---
<br>

6. Point free style Programming (Tacit)
7. Functors & Monads

## Cases

1. Branching: *if/else* with possible calculations
2. Context: arrows
3. Recursions
4. Determinism: exceptions
5. Purity: input, output, memoization

## How to run

Open a terminal session and

```bash
npm i
```

Open **app.ts** and ....

### Happy Exercising!

*No testing yet* ;)